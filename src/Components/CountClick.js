import { useEffect, useState } from "react"

const CountClick =() =>{

    const [count , setCount] = useState(0);

    const buttonClickHandler = () =>{
        setCount(count +1);
    }

    useEffect(() => {
        document.title = `You Clicked ${count} times`
    },[count]);
    return (
        <div>
            <p> You Clicked {count} times</p>
            <br/>
            <button onClick={buttonClickHandler}>Click me</button>
        </div>
    )
}

export default CountClick